import React from 'react';
import '../skeleton.css';

function Skeleton() {
    return (
        <div className="w-full my-4">
            <div className="flex flex-col justify-between items-center">
                <div data-placeholder className="h-16 w-full overflow-hidden rounded-md relative bg-slate-200" />
            </div>
        </div>
    )
}

export default Skeleton