import React from 'react';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

function Pagination() {
    return (
        <div className='flex justify-end'>
            <div className='w-10 bg-slate-100 h-10 pl-2 flex items-center justify-center cursor-pointer rounded-md border border-slate-200 hover:bg-slate-200'>
                <ArrowBackIosIcon className='text-gray-500' />
            </div>
            <div className='mx-2 w-10 bg-slate-100 h-10 flex items-center justify-center cursor-pointer rounded-md border border-slate-300 hover:bg-slate-200'>
                1
            </div>
            <div className='w-10 bg-slate-100 h-10 flex items-center justify-center cursor-pointer rounded-md border border-slate-200 hover:bg-slate-200'>
                <ArrowForwardIosIcon className='text-gray-500' />
            </div>
        </div>
    )
}

export default Pagination