import React, { useState, useEffect } from 'react';
import Skeleton from '../components/Skeleton';
import axios from 'axios';
import Pagination from '../components/Pagination';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

function Data() {

    const [loading, setLoading] = useState(false);
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetchUsers();
    }, [])

    const fetchUsers = async () => {
        setLoading(true);
        const res = await axios.get('https://jsonplaceholder.typicode.com/users');
        if (res.status === 200) {
            setLoading(false);
            setUsers(res.data);
        }
    }

    return (
        <section className='p-4'>
            <article>
                <div className='flex justify-between'>
                    <h1 className='text-xl text-blue-900 leading-7 border-b border-blue-900 inline-block'>Data</h1>
                    <div className='hidden lg:flex items-center'>
                        <p>Sorted By</p>
                        <ArrowDropDownIcon className='text-slate-400 cursor-pointer' />
                    </div>
                </div>
                {
                    true ? (
                        <>
                            <Skeleton />
                            <Skeleton />
                            <Skeleton />
                            <Skeleton />
                            <Skeleton />
                        </>
                    ) : (
                        <div className='my-4'>
                            {
                                users.map((user, index) => (
                                    <p key={index} className='py-2'>{index + 1}. {user.name}</p>
                                ))
                            }
                        </div>
                    )
                }
            </article>
            {
                !loading && (
                    <Pagination />
                )
            }
        </section>
    )
}

export default Data