import React from 'react';
import Header from './Header';
import Sidebar from './Sidebar';

function Layout({ children }) {
    return (
        <div className='h-screen'>
            <Header />
            <div className='flex h-full'>
                <Sidebar />
                <div className='flex-1 lg:flex-auto'>
                    {children}
                </div>
            </div>
        </div>
    )
}

export default Layout