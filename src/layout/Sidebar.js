import React from 'react';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';

function Sidebar() {
    return (
        <div className='w-60 p-4 box-border mx-4 hidden lg:flex lg:flex-col bg-slate-200'>
            <h1 className='text-xl text-blue-900 text-center w-full'>Filter</h1>
            <div className='my-4 group'>
                <div className='flex justify-between hover:bg-slate-50 group-hover:bg-slate-50 cursor-pointer p-4 rounded-md'>
                    <p>Drop Down Menu</p>
                    <KeyboardArrowRightIcon className='group-hover:rotate-90' />
                </div>
                <ul className='p-4 hidden group-hover:flex group-hover:flex-col bg-slate-50 rounded-md my-2'>
                    <li className='cursor-pointer p-2 hover:bg-slate-200 rounded-md'>Sub Menu Item</li>
                    <li className='cursor-pointer p-2 hover:bg-slate-200 rounded-md'>Sub Menu Item</li>
                    <li className='cursor-pointer p-2 hover:bg-slate-200 rounded-md'>Sub Menu Item</li>
                </ul>
            </div>
        </div>
    )
}

export default Sidebar