import React, { useState, Fragment } from 'react';
import StoreMeshLogo from '../assets/storemesh.png';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import { Dialog, Transition } from '@headlessui/react';
import AppsIcon from '@mui/icons-material/Apps';

function Header() {

	const [isOpen, setIsOpen] = useState(false);
	const [isShow, setIsShow] = useState(false);

	return (
		<nav className='relative'>
			<div className='w-full py-2 max-h-20 h-auto bg-slate-200 lg:bg-white flex items-center justify-between'>
				<div className='flex items-center'>
					<div className='my-1 flex flex-col items-end'>
						<img src={StoreMeshLogo} alt='Store Mesh Logo' className='h-12 w-auto' />
						<p className='px-2 text-xs tracking-[2px]'>Connecting Business</p>
					</div>
					<ul className='hidden lg:flex'>
						<li className='mx-2 px-4 py-2 hover:bg-slate-100 rounded-md cursor-pointer'>Menu 1</li>
						<li className='mx-2 px-4 py-2 hover:bg-slate-100 rounded-md cursor-pointer'>Menu 2</li>
						<li className='mx-2 px-4 py-2 hover:bg-slate-100 rounded-md cursor-pointer'>Menu 3</li>
					</ul>
				</div>
				{/* Desktop Menu */}
				<div className='hidden lg:flex lg:justify-between'>
					<div className='px-4 flex items-center'>
						<div className='relative'>
							<SearchIcon className='absolute translate-y-2/4 text-slate-400 ml-2' />
							<input placeholder='Search' className='rounded-md outline-none p-2 border border-blue-200 pl-8 bg-slate-100' />
						</div>
						<AppsIcon className='mx-4 cursor-pointer' />
						<div className='w-8 h-8 bg-slate-300 flex justify-center items-center rounded-full cursor-pointer hover:bg-slate-400'>
							<p className='text-xl'>A</p>
						</div>
					</div>
				</div>
				{/* End Desktop Menu */}
				{/* Mobile Menu */}
				<div className='px-4 py-2 lg:hidden'>
					<MenuIcon className='text-blue-900 cursor-pointer' onClick={() => setIsShow(!isShow)} />
					<div className='my-1'>
						<SearchIcon className='text-blue-900 cursor-pointer' onClick={() => setIsOpen(true)} />
					</div>
				</div>
				{/* End Mobile Menu */}
			</div>
			<ul className={`${isShow ? 'flex flex-col absolute' : 'hidden'} ' bg-white w-full p-4 shadow-md z-50`}>
				<li className='p-2 cursor-pointer hover:bg-slate-100 rounded-md'>Menu 1</li>
				<li className='p-2 cursor-pointer hover:bg-slate-100 rounded-md'>Menu 2</li>
				<li className='p-2 cursor-pointer hover:bg-slate-100 rounded-md'>Menu 3</li>
			</ul>
			<Transition show={isOpen} as={Fragment}>
				<Dialog open={isOpen} onClose={() => setIsOpen(false)} className="relative z-50">
					<Transition.Child
						as={Fragment}
						enter="ease-out duration-300"
						enterFrom="opacity-0"
						enterTo="opacity-100"
						leave="ease-in duration-200"
						leaveFrom="opacity-100"
						leaveTo="opacity-0"
					>
						<div className="fixed inset-0 bg-black/30" />
					</Transition.Child>
					<div className="fixed inset-0 flex items-center justify-center p-4">
						<Transition.Child
							as={Fragment}
							enter="ease-out duration-300"
							enterFrom="opacity-0 scale-95"
							enterTo="opacity-100 scale-100"
							leave="ease-in duration-200"
							leaveFrom="opacity-100 scale-100"
							leaveTo="opacity-0 scale-95"
						>
							<Dialog.Panel className="mx-auto max-w-sm w-full rounded bg-white p-4">
								<Dialog.Title className='text-xl text-blue-900'>Search Data</Dialog.Title>
								<div className='my-4'>
									<input className='w-full rounded-sm outline-none p-2 border border-blue-200' />
								</div>
								<div className='flex justify-end'>
									<button onClick={() => setIsOpen(false)} className='bg-blue-100 text-blue-800 px-4 py-2 mx-4 rounded-md hover:bg-blue-200'>Search</button>
									<button onClick={() => setIsOpen(false)} className='text-gray-500 hover:text-gray-700'>Cancel</button>
								</div>
							</Dialog.Panel>
						</Transition.Child>
					</div>
				</Dialog>
			</Transition>
		</nav >
	)
}

export default Header