import React from 'react';
import Layout from './layout/Layout';
import Data from './views/Data';

function App() {
	return (
		<Layout>
			<Data />
		</Layout>
	)
}

export default App